#Method checks integer format
def check_int(inte):
    while True:
        try:
            number = int(inte)
        except ValueError:
            inte = raw_input('Retype:> ')
        else:
            break
    return number

#Method checks the legality of interval
def check_interval(start, end, interval):
    while True:
        if start < end:
	    if interval > 0:
	        break
            else:
 		inte = raw_input('Positive interval:> ')
                interval = check_int(inte)
        else:
	    if interval < 0:
                break
            else:
                inte = raw_input('Negative interval:> ')
                interval = check_int(inte)
    return interval

#Input parameters
Start_number = raw_input('Start:> ')
start = check_int(Start_number)

End_number = raw_input('End:> ')
end = check_int(End_number)

Interval = raw_input('Interval:> ')
itv = check_int(Interval)
itv = check_interval(start, end, itv)

#Output
ran = range(start, end, itv)

for count in range(0, len(ran)- 1):
    print "{0} {1}".format(ran[count], ran[count + 1])


